from reg.categories import CategoryKey
from reg.main import validate_registration_request


def test_validate_no_consent():
    error = validate_registration_request("Follower", [CategoryKey.e_st], False)
    assert error == "Необходимо дать согласие на обработку введённых данных."


def test_validate_no_categories():
    error = validate_registration_request("Follower", [], True)
    assert error == "Необходимо выбрать хотя бы одну категорию."


def test_validate_duplicate_categories():
    error = validate_registration_request(
        "Follower", [CategoryKey.e_st, CategoryKey.e_la, CategoryKey.e_st], True
    )
    assert error == "Каждую категорию можно выбрать только один раз."


def test_validate_incompatible_categories():
    error = validate_registration_request(
        "Follower", [CategoryKey.e_st, CategoryKey.e_la, CategoryKey.d_st], True
    )
    assert error == (
        'Категория "E класс Стандарт" не совместима с категорией "D класс Стандарт".'
    )


def test_validate_solo_category_with_follower():
    error = validate_registration_request("Follower", [CategoryKey.hobby_solo], True)
    assert error == 'Для категории "Хобби Соло" не нужно вводить ФИО партнёрши.'


def test_validate_non_solo_category_without_follower():
    error = validate_registration_request("", [CategoryKey.e_st], True)
    assert error == "Для выбранных категорий необходимо ввести ФИО партнёрши."


def test_validate_valid_solo():
    error = validate_registration_request("", [CategoryKey.hobby_solo], True)
    assert error is None


def test_validate_valid_non_solo():
    error = validate_registration_request(
        "Follower", [CategoryKey.e_st, CategoryKey.e_la], True
    )
    assert error is None
