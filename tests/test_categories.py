from reg.categories import Categories, Category, CategoryKey


def test_keys_in_category_all_and_category_key_enum_match():
    assert len(CategoryKey) == len(Categories.all())

    for category_key in CategoryKey:
        category = Categories.get(category_key)
        assert category.key == category_key


def test_category_names_are_unique():
    previous_category_names = set()
    for category in Categories.all():
        assert category.name not in previous_category_names
        previous_category_names.add(category.name)


def test_incompatibilities_are_commutative():
    for category in Categories.all():
        for incompatible_category_key in category.incompatible:
            incompatible_category = Categories.get(incompatible_category_key)
            assert category.key in incompatible_category.incompatible


def test_solo_incompatible_with_non_solo():
    solo_categories: list[Category] = []
    non_solo_categories: list[Category] = []

    for category in Categories.all():
        collection = solo_categories if category.is_solo else non_solo_categories
        collection.append(category)

    for solo_category in solo_categories:
        for non_solo_category in non_solo_categories:
            assert non_solo_category.key in solo_category.incompatible
