import functools

from reg import utils


def test_get_number_case():
    g = functools.partial(utils.get_number_case, "заявка", "заявки", "заявок")
    test_cases = {
        "заявка": [1, 21, 101, 121],
        "заявки": [2, 3, 4, 23, 84, 102, 103, 104, 123, 184],
        "заявок": [0, 11, 12, 13, 14, 17, 100, 111, 112, 113, 114, 117],
    }
    for w, nums in test_cases.items():
        for n in nums:
            assert g(n) == w, f"Invalid case for number {n}"
            assert g(-n) == w, f"Invalid case for number {-n}"
