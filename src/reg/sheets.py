from google.oauth2 import service_account
from googleapiclient.discovery import build


class TableConnector:
    def __init__(self, credentials_file, spreadsheet_id, spreadsheet_range):
        credentials = service_account.Credentials.from_service_account_file(
            credentials_file,
            scopes=["https://www.googleapis.com/auth/spreadsheets"],
        )
        self.sheets = build("sheets", "v4", credentials=credentials).spreadsheets()
        self.spreadsheet_id = spreadsheet_id
        self.spreadsheet_range = spreadsheet_range

    def append(self, row):
        request = self.sheets.values().append(
            spreadsheetId=self.spreadsheet_id,
            range=self.spreadsheet_range,
            valueInputOption="RAW",
            insertDataOption="INSERT_ROWS",
            body=dict(values=[row]),
        )
        return request.execute()
