let namesDescr;
let leaderLabel;
let followerInput;
let followerQuestion;
let followerMarker;

let boxes = [];

function createReactiveSet() {
    return {
        value: new Set(),
        subscribers: {},
        subscribe: function(eventType, cb) {
            if (!this.subscribers[eventType]) {
                this.subscribers[eventType] = [];
            }
            this.subscribers[eventType].push(cb);
        },
        notify: function(eventType, value) {
            for (const cb of this.subscribers[eventType]) {
                cb(value);
            }
        },
        add: function(key) {
            this.value.add(key);
            this.notify("add", key);
        },
        delete: function(key) {
            this.value.delete(key);
            this.notify("delete", key);
        },
        has: function(key) {
            return this.value.has(key);
        },
    }
}

const checkedSet = createReactiveSet();

window.addEventListener("DOMContentLoaded", function() {
    namesDescr = document.getElementById("descr-names");
    leaderLabel = document.getElementById("label-leader");
    followerInput = document.getElementById("follower");
    followerQuestion = document.getElementById("question-follower");
    followerMarker = document.getElementById("follower-required-marker");

    for (const box of getBoxes()) {
        boxes.push({
            name: box.id,
            isSolo: "isSolo" in box.dataset,
            incompatible: new Set(box.dataset.incompatible.split(",")),
            element: box,
            notes: document.querySelectorAll(
                `#category-notes .note-text[data-category-key="${box.id}"]`
            ),
        });
    }

    for (const box of boxes) {
        initializeBox(box);
    }

    const form = document.getElementById("reg-form");
    form.setAttribute("novalidate", "");
    form.addEventListener("submit", handleSubmit);

    setTimeout(triggerBoxesManually, 10);
    setTimeout(triggerBoxesManually, 300);
});

function handleSolo(box) {
    if (!box.isSolo) {
        return;
    }

    if (isSoloCategoryChosen()) {
        setNamesForSolo();
    } else {
        setNamesForCouple();
    }
}

function updateCheckedSet(box) {
    if (box.element.checked) {
        checkedSet.add(box.name);
    } else {
        checkedSet.delete(box.name);
    }
}

function updateNotes(box) {
    if (box.element.checked) {
        for (const note of box.notes) {
            note.classList.remove("hidden");
        }
    } else {
        for (const note of box.notes) {
            note.classList.add("hidden");
        }
    }
}

function initializeBox(box) {
    box.element.addEventListener("change", function() {
        updateCheckedSet(box);
        handleSolo(box);
        updateNotes(box);
    });

    checkedSet.subscribe("add", function(value) {
        if (box.incompatible.has(value)) {
            disable(box.element);
        }
    });

    checkedSet.subscribe("delete", function() {
        let stillIncompatible = false;
        for (const incompatibleCategory of box.incompatible) {
            stillIncompatible |= checkedSet.has(incompatibleCategory);
        }
        if (!stillIncompatible) {
            enable(box.element);
        }
    });
}

function triggerBoxesManually() {
    for (const box of boxes) {
        const ev = new Event("change", {
            "bubbles": true,
        });
        box.element.dispatchEvent(ev);
    }
}

function isSoloCategoryChosen() {
    for (const box of boxes) {
        if (box.isSolo && box.element.checked) {
            return true;
        }
    }
    return false;
}

function questionId(categoryId) {
    return "question-" + categoryId;
}

function getBoxes() {
    return document.querySelectorAll("#categories-container input[type='checkbox']");
}

function disable(input) {
    input.disabled = true;
    const question = document.getElementById(questionId(input.id));
    question.classList.add("disabled-question");
}

function enable(input) {
    input.disabled = false;
    const question = document.getElementById(questionId(input.id));
    question.classList.remove("disabled-question");
}

function setNamesForCouple() {
    namesDescr.textContent = "Введите фамилию и имя партнёра и партнёрши.";
    leaderLabel.textContent = "Партнёр:";
    followerInput.disabled = false;
    followerInput.required = true;
    followerMarker.textContent = "*";
    followerQuestion.classList.remove("hidden");
}

function setNamesForSolo() {
    namesDescr.textContent = "Введите фамилию и имя солиста.";
    leaderLabel.textContent = "Солист:";
    followerInput.disabled = true;
    followerInput.value = "";
    followerQuestion.classList.add("hidden");
}

function highlight(elementId) {
    const el = document.getElementById(elementId);
    el.scrollIntoView({"behavior": "smooth"});

    el.classList.remove("bg-transition");
    el.classList.add("err-highlighted");

    setTimeout(function() {
        el.classList.add("bg-transition");
    }, 1000);
}

function valuePresent(inputId) {
    return !!document.getElementById(inputId).value;
}

function validate() {
    if (checkedSet.value.size === 0) {
        highlight("section-category");
        return false;
    }

    if (!valuePresent("leader")) {
        highlight("section-names");
        document.getElementById("leader").focus();
        return false;
    }

    if (!isSoloCategoryChosen() && !valuePresent("follower")) {
        highlight("section-names");
        document.getElementById("follower").focus();
        return false;
    }

    if (!valuePresent("contacts")) {
        highlight("section-contacts");
        document.getElementById("contacts").focus();
        return false;
    }

    if (!valuePresent("club")) {
        highlight("section-club");
        document.getElementById("club").focus();
        return false;
    }

    if (!valuePresent("teacher-1")) {
        highlight("section-teachers");
        document.getElementById("teacher-1").focus();
        return false;
    }

    if (!document.getElementById("consent").checked) {
        highlight("section-consent");
        document.getElementById("consent").focus();
        return false;
    }

    return true;
}

function handleSubmit(e) {
    if (!validate()) {
        e.preventDefault();
    }
}