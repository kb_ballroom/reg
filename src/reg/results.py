from dataclasses import dataclass

from sqlalchemy.orm import Session

from reg.utils import get_number_case

from .categories import Categories, Category
from .database import Entry


@dataclass
class ResultsRow:
    leader: str
    follower: str
    club: str
    teacher1: str
    teacher2: str


@dataclass
class ResultsSection:
    category: Category
    rows: list[ResultsRow]

    def count_descr(self):
        count = len(self.rows)
        word = get_number_case("заявка", "заявки", "заявок", count)
        return f"{count} {word}"


def get_results_sections(db: Session):
    sections = {
        category.key: ResultsSection(category, []) for category in Categories.all()
    }

    query = (
        db.query(Entry)
        .filter(Entry.hide.is_(False))
        .order_by(Entry.leader, Entry.follower)
    )

    for entry in query.all():
        row = ResultsRow(
            entry.leader,
            entry.follower,
            entry.club,
            entry.teacher1,
            entry.teacher2,
        )

        for category_key in entry.categories.split(","):
            sections[category_key].rows.append(row)

    return sections.values()
