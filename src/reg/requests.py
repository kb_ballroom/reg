import re

from fastapi import Form
from pydantic import BaseModel, ConfigDict, field_validator

from .categories import Categories, CategoryKey


class AddNewParticipantRequest(BaseModel):
    leader: str
    follower: str
    contacts: str
    club: str
    teacher1: str
    teacher2: str
    category: list[CategoryKey]
    consent: bool

    model_config = ConfigDict(str_strip_whitespace=True)

    @field_validator("leader", "follower", "club", "teacher1", "teacher2")
    @classmethod
    def remove_extra_whitespace(cls, v):
        return re.sub(r"\s+", " ", v)

    @classmethod
    def as_form(
        cls,
        leader: str = Form(...),
        follower: str = Form(""),
        contacts: str = Form(...),
        club: str = Form(...),
        teacher1: str = Form(...),
        teacher2: str = Form(""),
        category: list[CategoryKey] = Form([]),
        consent: bool = Form(...),
    ):
        return cls(
            leader=leader,
            follower=follower,
            contacts=contacts,
            club=club,
            teacher1=teacher1,
            teacher2=teacher2,
            category=category,
            consent=consent,
        )


def validate_registration_request(
    follower: str,
    category_keys: list[CategoryKey],
    consent: bool,
):
    if not consent:
        return "Необходимо дать согласие на обработку введённых данных."

    if not category_keys:
        return "Необходимо выбрать хотя бы одну категорию."

    category_keys_set = set(category_keys)
    if len(category_keys_set) != len(category_keys):
        return "Каждую категорию можно выбрать только один раз."

    for category_key in category_keys:
        category = Categories.get(category_key)

        if incompatible_category_keys := category.incompatible & category_keys_set:
            some_incompatible_category = Categories.get(
                next(iter(incompatible_category_keys))
            )
            return (
                f'Категория "{category.name}" не совместима с категорией'
                f' "{some_incompatible_category.name}".'
            )

        if category.is_solo and follower:
            return f'Для категории "{category.name}" не нужно вводить ФИО партнёрши.'

        if not category.is_solo and not follower:
            return "Для выбранных категорий необходимо ввести ФИО партнёрши."
