from ..categories import Categories
from ..database import Entry, SessionLocal


def export():
    db = SessionLocal()

    query = db.query(Entry).filter(Entry.hide.is_(False))

    lines = []

    for entry in query.all():
        category_keys = entry.categories.split(",")

        category_names = ", ".join(
            Categories.get(category_key).name for category_key in category_keys
        )

        teachers = (
            ", ".join([entry.teacher1, entry.teacher2])
            if entry.teacher2
            else entry.teacher1
        )

        values = [
            entry.date,
            entry.leader,
            entry.follower,
            entry.club,
            teachers,
            entry.contacts,
            category_names,
        ]

        line = "\t".join(val if val is not None else "" for val in values)
        lines.append(line)

    print("\n".join(lines))
