from collections import Counter
from collections.abc import Iterable
from enum import Enum
from typing import Annotated

from typer import Option

from ..database import Entry, SessionLocal


class Mode(str, Enum):
    fullname = "fullname"
    name_parts = "name-parts"


def normalize_name(name: str) -> str:
    parts = name.split()
    parts = [part.lower().replace("ё", "e").capitalize() for part in parts]
    return " ".join(parts)


def get_participant_full_names(entry: Entry) -> list[str]:
    return [name for name in [entry.leader, entry.follower] if name]


def get_participant_name_parts(entry: Entry) -> list[str]:
    parts = []

    for name in get_participant_full_names(entry):
        parts.extend(name.split())

    return parts


def get_names(entry: Entry, mode: Mode) -> Iterable[str]:
    if mode == Mode.fullname:
        names = get_participant_full_names(entry)
    elif mode == Mode.name_parts:
        names = get_participant_name_parts(entry)
    else:
        raise RuntimeError

    return map(normalize_name, names)


def duplicates(mode: Annotated[Mode, Option()] = Mode.fullname):
    db = SessionLocal()
    names_counter: Counter[str] = Counter()

    for entry in db.query(Entry).filter(Entry.hide.is_(False)).all():
        names = get_names(entry, mode)
        names_counter.update(names)

    for name, count in names_counter.most_common():
        if count == 1:
            break

        print(f"{name} - {count} occurrences")
