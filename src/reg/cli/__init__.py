from typer import Typer

from .duplicates import duplicates
from .export import export

app = Typer()
app.command()(export)
app.command()(duplicates)
