import logging
from datetime import datetime
from pathlib import Path
from urllib.parse import urlencode

import jinja2
from fastapi import BackgroundTasks, Depends, FastAPI, HTTPException, Request
from fastapi.exceptions import RequestValidationError
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session

from .categories import Categories, CategoriesView
from .config import State, settings
from .database import Base, Entry, SessionLocal, engine
from .middleware import NoCacheMiddleware
from .requests import AddNewParticipantRequest, validate_registration_request
from .results import get_results_sections
from .sentry import init_sentry
from .sheets import TableConnector
from .telegram import TelegramConnector

init_sentry()


front_path = Path(__file__).parent.absolute() / "frontend"
static_path = front_path / "static"
templates_path = front_path / "templates"


private_table_connector = TableConnector(
    settings.google_credentials_file,
    settings.private_sheet_id,
    settings.private_sheet_range,
)


telegram_connector = TelegramConnector(
    settings.tg_bot_token,
    settings.tg_chat_id,
)


def wrap_fallible_action(action, on_exception=None):
    def inner(*args, **kwargs):
        try:
            action(*args, **kwargs)
        except Exception as exc:
            if on_exception:
                on_exception(exc)

    return inner


def log_exception(message):
    def inner(_exc):
        logging.exception(message)

    return inner


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


Base.metadata.create_all(bind=engine)

app = FastAPI(openapi_url=None)

templates = Jinja2Templates(directory=templates_path, undefined=jinja2.StrictUndefined)


@app.exception_handler(404)
async def handle_error_not_found(req: Request, exc: HTTPException):
    return templates.TemplateResponse(
        "404.html", dict(request=req, contact_url=settings.contact_url), status_code=404
    )


@app.exception_handler(Exception)
async def exception_handler(req: Request, exc: Exception):
    logging.error("Exception caught in global handler", exc_info=exc)
    return templates.TemplateResponse(
        "server-error.html",
        dict(request=req, contact_url=settings.contact_url),
        status_code=500,
    )


@app.exception_handler(RequestValidationError)
async def validation_error_handler(req: Request, exc: RequestValidationError):
    return templates.TemplateResponse(
        "client-error.html",
        dict(
            error_message="",
            contact_url=settings.contact_url,
            request=req,
        ),
        status_code=422,
    )


@app.post("/submit")
def submit(
    req: Request,
    bg: BackgroundTasks,
    db: Session = Depends(get_db),
    data: AddNewParticipantRequest = Depends(AddNewParticipantRequest.as_form),
):
    if settings.state != State.reg_open:
        return templates.TemplateResponse(
            "closed-error.html",
            dict(
                contact_url=settings.contact_url,
                request=req,
            ),
            status_code=400,
        )

    if error := validate_registration_request(
        data.follower, data.category, data.consent
    ):
        return templates.TemplateResponse(
            "client-error.html",
            dict(
                error_message=error,
                contact_url=settings.contact_url,
                request=req,
            ),
            status_code=422,
        )

    date = datetime.now(settings.timezone).isoformat(timespec="milliseconds")

    db_entry = Entry(
        date=date,
        leader=data.leader,
        follower=data.follower,
        contacts=data.contacts,
        club=data.club,
        teacher1=data.teacher1,
        teacher2=data.teacher2,
        categories=",".join(data.category),
        hide=False,
    )

    db.add(db_entry)
    db.commit()

    category_names = ", ".join(Categories.get(key).name for key in data.category)
    teachers = (
        ", ".join([data.teacher1, data.teacher2]) if data.teacher2 else data.teacher1
    )
    participant = f"{data.leader} - {data.follower}" if data.follower else data.leader

    sheet_values = [
        date,
        data.leader,
        data.follower,
        data.club,
        teachers,
        data.contacts,
        category_names,
    ]
    bg.add_task(
        wrap_fallible_action(
            private_table_connector.append,
            on_exception=log_exception("Failed to save submission in Google Sheets"),
        ),
        sheet_values,
    )

    tg_values = [participant, data.club, teachers, category_names]
    bg.add_task(
        wrap_fallible_action(
            telegram_connector.notify,
            on_exception=log_exception("Failed to send Telegram notification"),
        ),
        tg_values,
    )

    return RedirectResponse(
        "success?" + urlencode(dict(whom=participant, where=category_names)),
        303,
    )


@app.get("/success")
async def success(req: Request, whom: str, where: str):
    return templates.TemplateResponse(
        "success.html",
        dict(
            whom=whom,
            where=where,
            contact_url=settings.contact_url,
            request=req,
        ),
    )


@app.get("/results")
def results(req: Request, db: Session = Depends(get_db)):
    sections = get_results_sections(db)

    return templates.TemplateResponse(
        "results.html",
        dict(request=req, sections=sections),
    )


@app.get("/rules")
async def rules():
    return RedirectResponse(settings.rules_url)


@app.get("/protocols")
async def protocols(req: Request):
    if not settings.protocols_url:
        return templates.TemplateResponse(
            "protocols-not-available.html", dict(request=req)
        )

    return RedirectResponse(settings.protocols_url)


@app.get("/about")
async def about():
    return RedirectResponse(settings.about_url)


@app.get("/")
async def index(req: Request):
    if settings.state == State.reg_finished:
        return templates.TemplateResponse(
            "index-closed.html", dict(request=req, contact_url=settings.contact_url)
        )
    elif settings.state == State.comp_finished:
        return templates.TemplateResponse(
            "index-finished.html", dict(request=req, contact_url=settings.contact_url)
        )

    return templates.TemplateResponse(
        "index.html",
        dict(
            request=req,
            contact_url=settings.contact_url,
            categories=CategoriesView.from_categories(),
        ),
    )


static_files = StaticFiles(directory=static_path)
app.mount("/static", static_files, name="static")

app.add_middleware(NoCacheMiddleware)
