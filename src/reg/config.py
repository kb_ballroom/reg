import zoneinfo
from enum import Enum
from pathlib import Path
from typing import Optional

from pydantic import HttpUrl, field_validator, model_validator
from pydantic_settings import BaseSettings, SettingsConfigDict


class State(str, Enum):
    reg_open = "reg_open"
    reg_finished = "reg_finished"
    comp_finished = "comp_finished"


class Settings(BaseSettings):
    # Путь до JSON-ключа сервисного аккаунта Google Cloud с доступом к Sheets API
    # Консоль Google Cloud: https://console.cloud.google.com/apis/dashboard
    # Пример настройки проекта на Google Cloud (до 2:35): https://youtu.be/fxGeppjO0Mg
    google_credentials_file: Path

    # ID Google таблицы
    # Можно найти в URL: docs.google.com/spreadsheets/d/<ID>/edit
    # В настройках доступа таблицы необходимо добавить сервисный аккаунт Google Cloud
    private_sheet_id: str

    # С каких ячеек Google таблицы начинать запись
    private_sheet_range: str = "A1"

    # ID Telegram чата для отправки уведомлений
    # Может быть личным чатом пользователя с ботом, либо группой или каналом с ботом
    # Можно найти через https://t.me/get_id_bot
    tg_chat_id: int

    # Токен Telegram бота
    # Создать бота и получить токен: https://t.me/botfather
    tg_bot_token: str

    # Настройки Sentry
    sentry_dsn: Optional[HttpUrl] = None

    # Отклонение временной зоны от UTC
    timezone: zoneinfo.ZoneInfo = zoneinfo.ZoneInfo("UTC")

    # Ссылки на группу ВК, положение, протоколы, постер турнира
    contact_url: HttpUrl
    rules_url: str
    protocols_url: str = ""
    about_url: str

    # URL базы данных SQLite3, например sqlite:///./db.sqlite3
    # Файл БД будет создан, если не существует
    db_url: str

    # Текущее состояние сайта
    state: State = State.reg_open

    model_config = SettingsConfigDict(env_file=".env")

    @model_validator(mode="after")
    def protocols_present_when_comp_finished(self) -> "Settings":
        if self.state == State.comp_finished and not self.protocols_url:
            raise ValueError(
                'protocols_url should be present when state is "comp_finished"',
            )
        return self

    @field_validator("timezone", mode="before")
    @classmethod
    def validate_timezone(cls, timezone) -> zoneinfo.ZoneInfo:
        if isinstance(timezone, str):
            try:
                return zoneinfo.ZoneInfo(timezone)
            except zoneinfo.ZoneInfoNotFoundError:
                raise ValueError(f"invalid timezone '{timezone}'")

        return timezone


settings = Settings()
