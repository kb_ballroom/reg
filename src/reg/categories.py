from collections.abc import Iterable
from enum import Enum

from pydantic import BaseModel, RootModel


class CategoryKey(str, Enum):
    debut_solo = "debut-solo"
    debut_couples = "debut-couples"
    hobby_solo = "hobby-solo"
    hobby_couples = "hobby-couples"
    e_st = "e-st"
    d_st = "d-st"
    cb_st = "cb-st"
    open_st = "open-st"
    e_la = "e-la"
    d_la = "d-la"
    cb_la = "cb-la"
    open_la = "open-la"

    def __str__(self):
        return str(self.value)


class Category(BaseModel):
    key: CategoryKey
    name: str
    is_solo: bool
    incompatible: set[CategoryKey]
    notes: list[str] = []


class CategoryView(BaseModel):
    name: str
    is_solo: bool
    incompatible: set[CategoryKey]
    notes: list[str]

    @classmethod
    def from_category(cls, c: Category) -> "CategoryView":
        return cls(
            name=c.name, is_solo=c.is_solo, incompatible=c.incompatible, notes=c.notes
        )


class CategoriesView(RootModel):
    root: dict[str, CategoryView]

    @classmethod
    def from_categories(cls):
        return cls(
            {k: CategoryView.from_category(v) for k, v in Categories.mapping.items()}
        )


_CATEGORIES_LIST = [
    Category(
        key=CategoryKey.debut_solo,
        name="Дебют Соло",
        is_solo=True,
        incompatible={
            CategoryKey.debut_couples,
            CategoryKey.hobby_solo,
            CategoryKey.hobby_couples,
            CategoryKey.e_st,
            CategoryKey.d_st,
            CategoryKey.cb_st,
            CategoryKey.open_st,
            CategoryKey.e_la,
            CategoryKey.d_la,
            CategoryKey.cb_la,
            CategoryKey.open_la,
        },
        notes=[
            (
                "Обратите внимание, что в категории «Дебют Соло» европейская и "
                "латиноамериканская программа исполняются и оцениваются вместе. "
                "Костюм должен подходить под обе программы."
            ),
            (
                "До участия в категории «Дебют Соло» допускаются только солисты, не "
                "участвовавшие в студенческих или профессиональных соревнованиях ранее "
                "осени 2023 года."
            ),
        ],
    ),
    Category(
        key=CategoryKey.debut_couples,
        name="Дебют Пары",
        is_solo=False,
        incompatible={
            CategoryKey.debut_solo,
            CategoryKey.hobby_solo,
            CategoryKey.hobby_couples,
            CategoryKey.e_st,
            CategoryKey.d_st,
            CategoryKey.cb_st,
            CategoryKey.open_st,
            CategoryKey.e_la,
            CategoryKey.d_la,
            CategoryKey.cb_la,
            CategoryKey.open_la,
        },
        notes=[
            (
                "Обратите внимание, что в категории «Дебют Пары» европейская и "
                "латиноамериканская программа исполняются и оцениваются вместе. "
                "Костюм должен подходить под обе программы."
            ),
            (
                "До участия в категории «Дебют Пары» допускаются только пары, в "
                "которых хотя бы один из партнёров не участвовал в студенческих или "
                "профессиональных соревнованиях ранее осени 2023 года, а второй из "
                "партнёров имеет не выше E класса."
            ),
        ],
    ),
    Category(
        key=CategoryKey.hobby_solo,
        name="Хобби Соло",
        is_solo=True,
        incompatible={
            CategoryKey.debut_solo,
            CategoryKey.debut_couples,
            CategoryKey.hobby_couples,
            CategoryKey.e_st,
            CategoryKey.d_st,
            CategoryKey.cb_st,
            CategoryKey.open_st,
            CategoryKey.e_la,
            CategoryKey.d_la,
            CategoryKey.cb_la,
            CategoryKey.open_la,
        },
        notes=[
            (
                "Обратите внимание, что в категории «Хобби Соло» европейская и "
                "латиноамериканская программа исполняются и оцениваются вместе. "
                "Костюм должен подходить под обе программы."
            ),
        ],
    ),
    Category(
        key=CategoryKey.hobby_couples,
        name="Хобби Пары",
        is_solo=False,
        incompatible={
            CategoryKey.debut_solo,
            CategoryKey.debut_couples,
            CategoryKey.hobby_solo,
            CategoryKey.e_st,
            CategoryKey.d_st,
            CategoryKey.cb_st,
            CategoryKey.open_st,
            CategoryKey.e_la,
            CategoryKey.d_la,
            CategoryKey.cb_la,
            CategoryKey.open_la,
        },
        notes=[
            (
                "Обратите внимание, что в категории «Хобби Пары» европейская и "
                "латиноамериканская программа исполняются и оцениваются вместе. "
                "Костюм должен подходить под обе программы."
            )
        ],
    ),
    Category(
        key=CategoryKey.e_st,
        name="E класс Стандарт",
        is_solo=False,
        incompatible={
            CategoryKey.debut_solo,
            CategoryKey.debut_couples,
            CategoryKey.hobby_solo,
            CategoryKey.hobby_couples,
            CategoryKey.d_st,
            CategoryKey.cb_st,
            CategoryKey.open_st,
        },
    ),
    Category(
        key=CategoryKey.d_st,
        name="D класс Стандарт",
        is_solo=False,
        incompatible={
            CategoryKey.debut_solo,
            CategoryKey.debut_couples,
            CategoryKey.hobby_solo,
            CategoryKey.hobby_couples,
            CategoryKey.e_st,
            CategoryKey.cb_st,
            CategoryKey.open_st,
        },
    ),
    Category(
        key=CategoryKey.cb_st,
        name="C+B класс Стандарт",
        is_solo=False,
        incompatible={
            CategoryKey.debut_solo,
            CategoryKey.debut_couples,
            CategoryKey.hobby_solo,
            CategoryKey.hobby_couples,
            CategoryKey.e_st,
            CategoryKey.d_st,
            CategoryKey.open_st,
        },
    ),
    Category(
        key=CategoryKey.open_st,
        name="Открытый класс Стандарт",
        is_solo=False,
        incompatible={
            CategoryKey.debut_solo,
            CategoryKey.debut_couples,
            CategoryKey.hobby_solo,
            CategoryKey.hobby_couples,
            CategoryKey.e_st,
            CategoryKey.d_st,
            CategoryKey.cb_st,
        },
    ),
    Category(
        key=CategoryKey.e_la,
        name="E класс Латина",
        is_solo=False,
        incompatible={
            CategoryKey.debut_solo,
            CategoryKey.debut_couples,
            CategoryKey.hobby_solo,
            CategoryKey.hobby_couples,
            CategoryKey.d_la,
            CategoryKey.cb_la,
            CategoryKey.open_la,
        },
    ),
    Category(
        key=CategoryKey.d_la,
        name="D класс Латина",
        is_solo=False,
        incompatible={
            CategoryKey.debut_solo,
            CategoryKey.debut_couples,
            CategoryKey.hobby_solo,
            CategoryKey.hobby_couples,
            CategoryKey.e_la,
            CategoryKey.cb_la,
            CategoryKey.open_la,
        },
    ),
    Category(
        key=CategoryKey.cb_la,
        name="C+B класс Латина",
        is_solo=False,
        incompatible={
            CategoryKey.debut_solo,
            CategoryKey.debut_couples,
            CategoryKey.hobby_solo,
            CategoryKey.hobby_couples,
            CategoryKey.e_la,
            CategoryKey.d_la,
            CategoryKey.open_la,
        },
    ),
    Category(
        key=CategoryKey.open_la,
        name="Открытый класс Латина",
        is_solo=False,
        incompatible={
            CategoryKey.debut_solo,
            CategoryKey.debut_couples,
            CategoryKey.hobby_solo,
            CategoryKey.hobby_couples,
            CategoryKey.e_la,
            CategoryKey.d_la,
            CategoryKey.cb_la,
        },
    ),
]


class Categories:
    mapping = {category.key: category for category in _CATEGORIES_LIST}

    @classmethod
    def get(cls, key: CategoryKey) -> Category:
        return cls.mapping[key]

    @classmethod
    def all(cls) -> Iterable[Category]:
        return cls.mapping.values()
