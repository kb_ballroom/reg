from starlette.datastructures import MutableHeaders


class NoCacheMiddleware:
    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):
        if scope["type"] != "http":
            return await self.app(scope, receive, send)

        async def new_send(event):
            if event["type"] == "http.response.start":
                headers = MutableHeaders(scope=event)
                headers.setdefault("cache-control", "no-cache")
            return await send(event)

        return await self.app(scope, receive, new_send)
