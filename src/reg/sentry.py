import sentry_sdk
from sentry_sdk.scrubber import DEFAULT_DENYLIST, EventScrubber

from .config import settings

PROJECT_DENYLIST = [
    "leader",
    "follower",
    "contacts",
    "teacher1",
    "teacher2",
]


def init_sentry():
    if not settings.sentry_dsn:
        return

    sentry_sdk.init(
        dsn=settings.sentry_dsn,
        event_scrubber=EventScrubber(DEFAULT_DENYLIST + PROJECT_DENYLIST),
    )
