def get_number_case(
    single: str, genitive_single: str, genitive_plural: str, count: int
):
    count = abs(count)
    last_two_digits = count % 100

    if 11 <= last_two_digits <= 14:
        return genitive_plural

    last_digit = last_two_digits % 10

    if last_digit == 1:
        return single

    if 2 <= last_digit <= 4:
        return genitive_single

    return genitive_plural
