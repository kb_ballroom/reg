from requests import Session


class TelegramConnector:
    def __init__(self, token, chat_id):
        self.chat_id = chat_id
        self.session = Session()
        self.method_url = f"https://api.telegram.org/bot{token}/sendMessage"

    def notify(self, values):
        text = "\n".join(values)
        args = dict(chat_id=self.chat_id, text=text)
        resp = self.session.post(self.method_url, json=args, timeout=10)

        if resp.status_code != 200 or not resp.json()["ok"]:
            raise Exception(
                "Failed to send Telegram notification. "
                f"Status: `{resp.status_code}`. Content: `{resp.content}`"
            )
